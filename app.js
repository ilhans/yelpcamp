var express         = require("express"),
    app             = express(),
    bodyParser      = require("body-parser"),
    mongoose        = require("mongoose"),
    methodOverride  = require("method-override"),
    seedDB          = require("./seeds.js"),
    passport        = require("passport"),
    localStrategy   = require("passport-local"),
    User            = require("./models/user");

var commentRoutes   = require("./routes/comments"),
    campgroundRoutes= require("./routes/campgrounds"),
    indexRoutes     = require("./routes/index");
    
mongoose.connect("mongodb://localhost/yelp_camp");
app.use(bodyParser.urlencoded( { extended: true}));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(methodOverride("_method"));
// seedDB();

// PASSPORT CONFIGURATION
app.use(require("express-session")({
    secret: "Once again Rusty wins cutest dog",
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new localStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// Send currentUser variable to every route
app.use(function(req, res, next) {
    res.locals.currentUser = req.user;
    next();
});

// ======== ALL DEFINED ROUTES =========
app.use(indexRoutes);
app.use(campgroundRoutes);
app.use(commentRoutes);

// ======== ALL OTHER ROUTES =========
app.get("/*", function(req, res) {
    res.send("404 Not Found"); 
});

app.listen(process.env.PORT, process.env.IP, function() {
    console.log("Application has started!");
});