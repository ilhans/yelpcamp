var express     = require("express");
var router      = express.Router();
var Campground  = require("../models/campground");

// INDEX    /campgrounds    GET     Display a list of all campgrounds
router.get("/campgrounds", function(req, res) {
    Campground.find({}, function(error, campgrounds) {
       if(error) {
           console.log(error);
       } else {
           res.render("campgrounds/index", { campgrounds: campgrounds });
       }
    });
});

// NEW    /campgrounds/new    GET     Displays form to make a new campground

router.get("/campgrounds/new", isLoggedIn, function(req, res) {
    res.render("campgrounds/new");
});

// CREATE    /campgrounds    POST     Add new campground to database

router.post("/campgrounds", isLoggedIn, function(req, res) {
    var newCampSite = {
            name: req.body.name,
            image: req.body.image,
            description: req.body.description,
            author: {
                id: req.user._id,
                username: req.user.username
            }
        };
    Campground.create(newCampSite, function(error, campground) {
        if(error) {
            console.log(error);
        } else {
            res.redirect("/campgrounds");
        }
    });
});

// SHOW    /campgrounds/:id    GET     Show info about one specific campground

router.get("/campgrounds/:id", function(req, res) {
    
    Campground.findById(req.params.id).populate("comments").exec(function(error, campground) {
        if(error) {
            console.log(error);
        } else {
            res.render("campgrounds/show", { campground: campground});
        }
    });
});

// EDIT     /campgrounds/:id/edit   GET Edit a specific campground
router.get("/campgrounds/:id/edit", isOwnerOfCampground, function(req, res) {
    Campground.findById(req.params.id, function(err, campground){
        if(err) {
            console.log("err");
            res.redirect("back");
        } else {
            res.render("campgrounds/edit", {campground: campground});
        }
    });
});

// UPDATE   /campgrounds/:id        PUT Update that specific campground
router.put("/campgrounds/:id", isOwnerOfCampground, function(req, res) {
    Campground.findByIdAndUpdate(req.params.id, req.body.campground, function(err, updatedCampground){
        if(err) {
            console.log(err);
        } else {
            res.redirect("/campgrounds/"+req.params.id);
        }
    }) 
});

// DESTROY /campgrounds/id      DELETE Delete a specific item
router.delete("/campgrounds/:id", isOwnerOfCampground, function(req, res){
     Campground.findByIdAndRemove(req.params.id, function(err) {
         if(err){
             console.log(err);
             res.redirect("/campgrounds");
         } else {
             res.redirect("/campgrounds");
         }
     });
});

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    res.redirect("/login");
}

function isOwnerOfCampground(req, res, next) {
    if(req.isAuthenticated()) {
        Campground.findById(req.params.id, function(err, campground){
            if(err) {
                console.log(err);
                res.redirect("back");
            } else {
                if(campground.author.id.equals(req.user._id)) {
                    next();
                } else {
                    res.redirect("back");
                }
            }
        });
    } else {
        res.redirect("back");
    }
}

module.exports = router;